<?php

class Logger{

    function insertRequest($data){
        $diretorio = __DIR__.'/log/request';
        $time = date('d-m-Y h:i:s');
        $log = fopen("$diretorio",'a');
        $data = "{'Data':'$time','Request': '$data',\n'URI': '{$_SERVER['REQUEST_URI']}'}\n";
        fwrite($log, $data);
    }

    function insertStatusDB($data, $sql, $status){
        $data = json_encode($data);
        $diretorio = __DIR__.'/log/statusDB';
        $time = date('d-m-Y h:i:s');
        $log = fopen("$diretorio",'a');
        $data = "{'Data':'$time', 'Request': '$data',\n'URI': '{$_SERVER['REQUEST_URI']}',\n'Query': '$sql', \n'Status': '$status'}\n";
        fwrite($log, $data);
    }
}