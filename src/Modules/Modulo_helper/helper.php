<?php

class Helper{

    function criaXML($metodo,$array){
        $xml = new SimpleXMLElement('<xml/>');
        $login = $xml->addChild($metodo);
        foreach ($array as $chave => $valor) {
            $login->addChild($chave, $valor);
        }
        return $xml->asXML();
    }

    function toJson($data){
        if(is_object($data)){
            return json_encode(($data),1);
        }else{
            return json_encode($data);
        } 
    }
}
?>
