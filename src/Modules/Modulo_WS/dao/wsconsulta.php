<?php



class ConsultaWS{

    protected $url;
    protected $credor;
    protected $soap;
    protected $ip;
    protected $telefone;
    protected $dao;
    protected $cpf;    

    function __construct($url, $credor, $cpf) {
        $this->url = $url;
        $this->credor = $credor;
        $this->cpf = $cpf;                  
        $this->soap = new SoapClient($url);      
    }

    function consultaWS($xmlRequest, $metodo){       
        try{
            ini_set("default_socket_timeout", 30);
            $response = $this->soap->__soapCall($metodo, array($xmlRequest));             
        }catch (SoapFault $e){
            return simplexml_load_string(
                parent::criaXML(
                    'retorno',
                    $retorno= array(
                        'Status' => "$e->faultstring"
                    )
                )
            );
        } 
        return simplexml_load_string(preg_replace('/&(?!;{6})/', '&amp;', $response));
    }

}

