<?php


class ControllerWS extends Helper{
    
    protected $connWS;
    protected $app;
    protected $credor;
    protected $url;
    protected $cpf;
    protected $dao;
    protected $chave;
    protected $live;
  
    function __construct($url, $credor, $cpf, $app, $chave, $live){

        $this->url = $url;
        $this->credor = $credor;
        $this->app = $app;
        $this->cpf = $cpf;        
        $this->chave = $chave;    
        $this->live = $live;    
        $this->connWS = new ConsultaWS($url, $credor, $cpf);
        $this->dao = new Dao();

    }

    function getLogin($ip=NULL){

        $xml_array = array(
            'cpfcnpj' => $this->cpf,
            'codcredor' => $this->credor,
            'app' => $this->app,
            'empresa' => 'MALTA',
            'ip' => $ip,
            'dispositivo' => (string) $this->app,
            'mobile' => 'true'     
        ); 
        $xml = parent::criaXML('login', $xml_array);          
        $resultLogin = $this->connWS->consultaWS($xml, 'wsSolicitarLogin'); 
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('login',parent::toJson($resultLogin), $this->chave);
        return $resultLogin;
    }

    function getContrato($resultLogin=NULL){

        $xml_array = array(
            'cpfcnpj' => $this->cpf,
            'contrato' => '',
            'codcredor' => (string)$this->credor,
            'codclien' => (string) $resultLogin->codclien,
            'app' => (string) $this->app,
            'sessionid' => '',
            'idmarca' => ''
        ); 

        $xml = parent::criaXML('consultacontrato', $xml_array);
        $resultContrato = $this->connWS->consultaWS($xml, 'wsConsultaContrato');     
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('contrato',parent::toJson($resultContrato), $this->chave);   
        return $resultContrato;
    }

    function getProposta($numContrato){

        $xml_array = array(
            'cpfcnpj' => (string) $this->cpf,
            'contrato' => (string) $numContrato,
            'codcredor' => (string) $this->credor,     
            'app' => (string) $this->app
        ); 
       
        $xml = parent::criaXML('consultaproposta', $xml_array); 
        $resultProposta = $this->connWS->consultaWS($xml, 'wsConsultaProposta');
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('proposta',parent::toJson($resultProposta), $this->chave);
        return $resultProposta;

    }
    
    function getBoleto($idProposta, $numContrato, $modo ){

        $xml_array = array(
            'id' => (string) $idProposta,
            'cpfcnpj' => (string) $this->cpf,
            'contrato'  => (string) $numContrato,
            'codcredor' => (string) $this->credor,
            'modo'  =>  (string) $modo,                      
            'app' => (string) $this->app
        ); 

        $xml = parent::criaXML('gerarboleto', $xml_array); 
        $xmlBoleto = $this->connWS->consultaWS($xml, 'wsGerarBoleto');
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('boleto',parent::toJson($xmlBoleto), $this->chave);
        return $xmlBoleto;
    }

    function get2Via($contrato){

        $xml_array = array(
            'cpfcnpj' => (string) $this->cpf,         
            'contrato' => (string) $contrato,
            'codcredor' => (string) $this->credor,
            'app' => (string) $this->app           
        );

        $xml = parent::criaXML('consultaboleto', $xml_array); 
        $xmlBoleto = $this->connWS->consultaWS($xml, 'wsConsultaBoleto');
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('2via',parent::toJson($xmlBoleto), $this->chave);
        return $xmlBoleto;
    }

    function metodoPagamento($ip, $idProposta, $numContrato, $tipo, $anoValidade, $bandeira, $codigoSeguranca, $mesValidade, $customerName, $numCartao){

        $retorno = $this->getBoleto($idProposta, $this->cpf, $numContrato, 1);

        $codCalc = $retorno->nossonumero;

        $xml_array = array(

            'ip' => $ip,
            'app' => (string) $this->app,
            'id' => (string) $idProposta,
            'cpfcnpj' => $this->cpf,
            'contrato' => $numContrato,
            'codcredor' => (string)$this->credor,
            'tipo' => (string)$tipo,
            'anovalidade' => (string)$anoValidade,
            'bandeira' => (string)$bandeira,
            'codigodeseguranca' => (string)$codigoSeguranca,
            'codigodocalculo' => (string)$codCalc,       
            'mesvalidade' => (string)$mesValidade,
            'nomeportador' => (string)$customerName,
            'numerocartao' => (string)$numCartao

        );

        $xml = parent::criaXML('pagamento', $xml_array); 
        $xmlRetorno = $this->connWS->consultaWS($xml, 'wsPagamento');
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('pagamento',parent::toJson($xmlRetorno), $this->chave);
        return $xmlRetorno->retornotexto;
    }

    function getDivida($numContrato){

        $xml_array = array(
            'cpfcnpj' => (string) $this->cpf,
            'contrato' => (string) $numContrato,
            'codcredor' => (string) $this->credor,     
            'app' => (string) $this->app
        );

        $xml = parent::criaXML('consultadivida', $xml_array);
        $resultDivida = $this->connWS->consultaWS($xml, 'wsConsultaDivida');   
        if(!is_null($this->chave) && $this->live===0)$this->dao->insertRegistro('divida',parent::toJson($resultDivida), $this->chave);  
        return $resultDivida;
    }

    function doTask(){

        $resultLogin = $this->getLogin();

        if($resultLogin){
            $this->dao->insertRegistro('login',parent::toJson($resultLogin), $this->chave);
            $resultContrato = $this->getContrato($resultLogin);
            if($resultContrato){
                $this->dao->alterRegistro('contrato', parent::toJson($resultContrato), $this->chave);
                $resultDivida = $this->getDivida($resultContrato->contratos->contrato->numcontrato);
                $resultProposta = $this->getProposta($resultContrato->contratos->contrato->numcontrato);
                $this->dao->alterRegistro('divida', parent::toJson($resultDivida), $this->chave);
                $this->dao->alterRegistro('proposta', parent::toJson($resultProposta), $this->chave);
                return array($resultLogin, $resultContrato, $resultDivida, $resultProposta);
            }
        }

    }


}


