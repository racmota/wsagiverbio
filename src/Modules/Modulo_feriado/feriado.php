<?php

function validaFeriado($data){
    if (strlen($data)<5 && is_numeric($data)) {
        $validador=1;
        $year = $data;
    }else{
        $validador=0;
        $data2 = explode('-', $data);
        $year = $data2[0];
        $month = $data2[1];
        $day = $data2[2];
        if(checkdate($month,$day,$year)){
        }else{
            return(('Erro - Data invalida Favor informar no modelo correto "ANO-MES-DIA"'));
            exit;
        }
    }
    date_default_timezone_set('America/Sao_Paulo');
    $a = $year%19;
    $b = (int)($year/100);
    $c = $year%100;
    $d = (int)($b/4);
    $e = $b%4;
    $f = (int)(($b+8)/25);
    $g = (int)(($b-$f+1)/3);
    $h = (19*$a+$b-$d-$g+15)%30;
    $i = (int)($c/4);
    $k = $c%4;
    $L = (32+2*$e+2*$i-$h-$k)%7;
    $m = (int)(($a+11*$h+22*$L)/451);
    $month = (int)(($h+$L-7*$m+114)/31);
    $day = 1+($h+$L-7*$m+114)%31;
    try {
        $pascoa=date_create($year."-".$month."-".$day)->format('Y-m-d');
        if (date('L', mktime(0, 0, 0, 1, 1, $year))==0) {
            $carnaval=date_create(date('Y-m-d', strtotime($pascoa.'- 47 days')))->format('Y-m-d');
        }else{
            $carnaval=date_create(date('Y-m-d', strtotime($pascoa.'- 48 days')))->format('Y-m-d');
        }
        $sexta=date_create(date('Y-m-d',strtotime($pascoa.'- 2 days')))->format('Y-m-d');
        $pentecostes = date_create(date('Y-m-d',strtotime($pascoa.'+ 49 days')))->format('Y-m-d');
        $trindade = date_create(date('Y-m-d',strtotime($pentecostes.'+ 7 days')))->format('Y-m-d');
        $corpus = date_create(date('Y-m-d',strtotime($trindade.'+ 4 days')))->format('Y-m-d');
    } catch(Exception $e) {
        return(("Erro no formato de data -".$e));
        exit;
    }
    $feriados =[
        'ANO NOVO' => $year.'-01-01',
        'TIRADENTES' => $year.'-04-21',
        'DIA DO PROLETARIADO' => $year.'-05-01',
        'INDEPENDENCIA' => $year.'-09-07',
        'NOSSA SENHORA' => $year.'-10-12',
        'FINADOS' => $year.'-11-02',
        'PROCLAMAÇÂO DA REPUBLICA' => $year.'-11-15',
        'NATAL' => $year.'-12-25',
        'PASCOA' => $pascoa,
        'CARNAVAL' => $carnaval,
        'SEXTA-FEIRA SANTA' => $sexta,
        'CORPUS CRIST' => $corpus
    ];
    if($validador == 1){
        return(($feriados));
    }else{
        return((in_array("$data", $feriados)));
    }
}