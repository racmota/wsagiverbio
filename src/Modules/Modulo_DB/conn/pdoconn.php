<?php

class Conexao{
    
    private static $conn;
    private static $conn2;

    private function __construct(){}

    public static function getConection($host, $usr, $pass){
	    try{
    		if(is_null(self::$conn)){
                self::$conn = new PDO("mysql:host=172.26.120.59;dbname=malta_new", "verbio", "verbio");
	            self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	            self::$conn->setAttribute(PDO::ATTR_AUTOCOMMIT, FALSE);
            }
        }catch(PDOException $e){
            self::$conn ='ERROR: ' . $e->getMessage();
        }finally{
            try{
                if(is_null(self::$conn2) && !is_null($host) && !is_null($usr) && !is_null($pass)){
                    self::$conn2 = new PDO("mysql:host=$host;dbname=malta_new", "$usr", "$pass");
                    self::$conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    self::$conn2->setAttribute(PDO::ATTR_AUTOCOMMIT, FALSE);
                }
                return array(self::$conn, self::$conn2);   
            }catch(PDOException $e){
                return array(self::$conn, self::$conn2 = NULL);
            }
        }
    }
}
