<?php


class ControllerDB{    

    protected $dao;

    function __construct(){
        $this->dao = new Dao();
    }
  
    function setClient($callResult){
        $clientData = array ();
        foreach ($callResult['metrics']['cliente'] as $key => $value) {
            $clientData[$key] = $value;
        }
        $retornoClientID = $this->dao->handleInsert($clientData, 'insertClient');
        return $retornoClientID;
    }

    function setCall($callResult){
        $callData = array();
        foreach ($callResult['metrics']['chamada'] as $key => $value) {
            if($key=='idClient'){
                $callData['T_info_client_id_client'] = $value;
            }else{
                $callData[$key] = $value;
            }
        }
        $callData['server'] = $_SERVER['REMOTE_ADDR'];
        $retornoCallID = $this->dao->handleInsert($callData, 'insertCall');
        return $retornoCallID;
    }

    function setProduct($callResult){
        $productData = array();
        foreach ($callResult['metrics']['produto'] as $key => $value) {
            if($key=='idClient'){
                $productData['T_info_client_id_client'] = $value;
            }else if($key=='idCall'){
                $productData['T_info_call_id_call'] = $value;
            }else if($key=='originalDebitDate' && $value== NULL || $value==0 ){
            }else{
                $productData[$key] = $value;
            }
        }
        $retornoProdutoID = $this->dao->handleInsert($productData, 'insertProduct');
        return $retornoProdutoID;
    }

    function setData($callResult){
        $clientData = array ();
        foreach ($callResult['metrics']['cliente'] as $key => $value) {
            $clientData[$key] = $value;
        }
        $callData = array();
        foreach ($callResult['metrics']['chamada'] as $key => $value) {
            $callData[$key] = $value;
        }
        $callData['server'] = $_SERVER['REMOTE_ADDR'];
        $productData = array();
        foreach ($callResult['metrics']['produto'] as $key => $value) {
            if($key=='originalDebitDate' && $value== NULL || $value==0 ){
            }else{
                $productData[$key] = $value;
            }
        }
        $result = $this->dao->insertData($clientData, $callData, $productData);
        return $result;
    }
}

?>
