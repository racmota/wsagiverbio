<?php

class Dao extends Logger{

    protected $conns;
    private $conn;
    private $size;
    private $iteration;

    function __construct($host=HOST, $usr=USER_NAME, $pass=PASSWORD){
        $this->conns = Conexao::getConection($host, $usr, $pass);
        $this->iteration = 0;
    }
  
    function insertRegistro($column,$response, $chave){
        $this->conn->beginTransaction();
        try{                   
            $sql = "INSERT INTO registro (`$column`, `chave`) VALUES ('$response', '$chave')";   
            $stmt = $conn->prepare($sql);  
            $result=$stmt->execute();
            $conn->commit();
            parent::insertStatusDB($response, $sql, $result);
        } catch (PDOException $e){
            parent::insertStatusDB($response, $sql, $e->getMessage());
            return $e->getMessage();
        }
    }  
   
    function alterRegistro($params, $values, $chave){
        $this->conn->beginTransaction();             
        try{    
            $sql = "UPDATE registro SET $params = '$values' WHERE chave = '$chave'";  
            $stmt = $this->conn->prepare($sql);  
            $result = $stmt->execute();
            $this->conn->commit(); 
            parent::insertStatusDB($params, $sql, $result);
        } catch (PDOException $e){
            parent::insertStatusDB($params, $sql, $e->getMessage());
            $this->conn->rollback();
            return $e->getMessage(); 
        }
    }
    
    function insertClient(Array $clientData){
        try{                
            $sql = "SELECT id_client FROM t_info_client WHERE cpf = '{$clientData['cpf']}'";
            $stmt = $this->conn->prepare($sql);
            $result = $stmt->execute();
            $count = $stmt->rowCount();
            if($count==0){            
                $params_fields = "`".implode("`, `", array_keys($clientData))."`";
                $params_values = ':'.implode(', :', array_keys($clientData));
                $sql = "INSERT INTO t_info_client ($params_fields) VALUES ($params_values)";
                $stmt = $this->conn->prepare($sql);            
                foreach ($clientData as $key => $value) {
                    $stmt->bindValue(":$key", $value);
                }                
                $result = $stmt->execute();           
                $id = $this->conn->lastInsertId();
                parent::insertStatusDB($clientData, $sql, $result);
                return array($result, $id);
            }else{
                $results = $stmt->fetchALL(PDO::FETCH_ASSOC);      
                parent::insertStatusDB($clientData, $sql, $result);
                return array(true, $results[0]['id_client']);
            }
        } catch (PDOException $e){
            parent::insertStatusDB($clientData, $sql, $e->getMessage());
            return array(false, $e->getMessage());
        }             
    }

    function insertCall(Array $callData){
        try{
            $params_fields = "`".implode("`, `", array_keys($callData))."`";
            $params_values = ':'.implode(', :', array_keys($callData));
            $sql = "INSERT INTO t_info_call ($params_fields) VALUES ($params_values)";
            $stmt = $this->conn->prepare($sql);  
            foreach ($callData as $key => $value) {
                $status = $stmt->bindValue(":$key", $value);
            }
            $result = $stmt->execute();    
            $idCall = $this->conn->lastInsertId();
            parent::insertStatusDB($callData, $sql, $result);
            return array($result, $idCall);
        }catch(PDOException $e){
            parent::insertStatusDB($callData, $sql, $e->getMessage());            
            return array(false, $e->getMessage());
        }
    }

    function insertProduct(Array $productData){
        try{
            $params_fields = "`".implode("`, `", array_keys($productData))."`";
            $params_values = ':'.implode(', :', array_keys($productData));
            $sql = "INSERT INTO t_info_product ($params_fields) VALUES ($params_values)";
            $stmt = $this->conn->prepare($sql); 
            foreach ($productData as $key => $value) {
                $stmt->bindValue(":$key", $value);
            }
            $result = $stmt->execute();
            parent::insertStatusDB($productData, $sql, $result);
            return $result;
        }catch(PDOException $e){
            parent::insertStatusDB($productData, $sql, $e->getMessage());  
            return array(false, $e->getMessage());
        }
    }

    function insertData(Array $clientData, Array $callData, Array $productData){
        foreach($this->conns as $conn){
            $this->iteration++;
            if(!is_null($conn)){   
                $this->conn = $conn; 
                $this->conn->beginTransaction();  
                $result = $this->insertClient($clientData);
                if($result[0]){
                    $callData['T_info_client_id_client'] = $result[1];
                    $result = $this->insertCall($callData);
                    if($result[0]){
                        $productData['T_info_call_id_call'] = $result[1];
                        $productData['T_info_client_id_client'] = $callData['T_info_client_id_client'];
                        $result = $this->insertProduct($productData);
                        if($result){
                            $this->conn->commit();
                            $this->conn=null;
                            $insertResult[$this->iteration] = $result;
                        }else{
                            $this->conn->rollback();
                            $this->conn=null;
                            $insertResult[$this->iteration] = $result;
                        }
                    }else{
                        $this->conn->rollback();
                        $this->conn=null;
                        $insertResult[$this->iteration] = $result;
                    }
                }else{
                    $this->conn->rollback();
                    $this->conn=null;
                    $insertResult[$this->iteration] = $result;
                }
            }
        }
        return $insertResult;
    }

    function handleInsert(Array $data, $method){
        if($method == 'insertCall' && is_array($data['T_info_client_id_client'])){
            $idClient = $data['T_info_client_id_client']; 
        }else if($method == 'insertProduct' && is_array($data['T_info_call_id_call'])){
            $idClient = $data['T_info_client_id_client'];
            $idCall = $data['T_info_call_id_call'];
        }
        foreach($this->conns as $conn){
            $this->iteration++;
            if(!is_null($conn)){
                $this->conn = $conn;
                $this->conn->beginTransaction();
                if(isset($idClient) && $method == 'insertCall'){
                    $data['T_info_client_id_client'] = $idClient['id'.$this->iteration];
                }else if(isset($idCall) && $method == 'insertProduct'){
                    $data['T_info_call_id_call'] = $idCall['id'.$this->iteration];
                    $data['T_info_client_id_client'] = $idClient['id'.$this->iteration];
                }
                $insertResult[$this->iteration] = call_user_func(array($this, $method), $data);
                if($insertResult[$this->iteration]){
                    $this->conn->commit();
                }else{
                    $this->conn->rollback();
                }
            }
        }
        return $insertResult;
    }
}
