<?php
    class InitTask extends Helper{

        static function select($function, $params){
            switch($function){
                case 'client':
                    if(isset($params['live'])) unset($params['live']);
                    $controllerDB = new ControllerDB();
                    $retorno = $controllerDB->setClient($params);    
                    return $retorno;
                break;
                case 'call':
                    if(isset($params['live'])) unset($params['live']);
                    $controllerDB = new ControllerDB();
                    $retorno = $controllerDB->setCall($params);    
                    return $retorno;
                break;
                case 'product':
                    if(isset($params['live'])) unset($params['live']);
                    $controllerDB = new ControllerDB();
                    $retorno = $controllerDB->setProduct($params);    
                    return $retorno;
                break;
                case 'getLogin':
                    $controllerWS = new ControllerWS($params['url'], $params['credor'], $params['cpf'], $params['app'], $params['chave'], $params['live']);
                    return parent::toJson($resultLogin = $controllerWS->getLogin());
                break;
                case 'getContrato':
                    $controllerWS = new ControllerWS($params['url'], $params['credor'], $params['cpf'], $params['app'], $params['chave'], $params['live']);
                    return parent::toJson($controllerWS->getContrato());
                break;
                case 'getProposta':
                    $controllerWS = new ControllerWS($params['url'], $params['credor'], $params['cpf'], $params['app'], $params['chave'], $params['live']);
                    return parent::toJson($controllerWS->getProposta($params['numcontrato']));
                break;
                case 'getDivida':
                    $controllerWS = new ControllerWS($params['url'], $params['credor'], $params['cpf'], $params['app'], $params['chave'], $params['live']);
                    return parent::toJson($controllerWS->getDivida($params['numcontrato']));
                break;

                case 'setCall':
                    if(isset($params['live'])) unset($params['live']);
                    $controllerDB = new ControllerDB();
                    $retorno = $controllerDB->setData($params);    
                    return $retorno;
                break;
                case 'all':
                    if(isset($params['live'])) $params['live'] = NULL;
                    $controllerWS = new ControllerWS($params['url'], $params['credor'], $params['cpf'], $params['app'], $params['chave'], $params['live']);
                    $retorno = $controllerWS->doTask();
                    return $retorno;
                break;
                case 'feriado':
                    $data = validaFeriado($params['data']);
                    return $data;
                break;

           }
        }

    }






?>