<?php


class App{

    public $routeCollection;

    function __construct(){
        $this->routeCollection = new RoutesCollection();
    }
  
    function get($path, $callback){
        $this->routeCollection->add('get', "/api$path", $callback);
    }

    function post($path, $callback){
        $this->routeCollection->add('post', "/api$path", $callback);      
    
    }

}





