<?php

    class RoutesCollection{

        private $routes_post = [];
        private $routes_get = [];

        function add($method, $uri, $callback){
            switch($method){
                case 'post':
                    $this->addPost($uri, $callback);
                break;
                case 'get':
                    $this->addGet($uri, $callback);
                break;
                default:
                    echo 'Metodo não cadastrado';
                break;
            }
        }

        protected function addPost($uri, $callback){
            $this->routes_post[$uri] = $callback;
        }

        protected function addGet($uri, $callback){
            $this->routes_get[$uri] = $callback;
        }

        function findRoute($uri, $method){
            if($method=='POST'){
                if(array_key_exists($uri, $this->routes_post)){
                    http_response_code(200);
                    return $this->routes_post[$uri];
                }else{
                    http_response_code(404);
                    echo 'Rota não encontrada';
                }
            }else if($method == 'GET'){
                if(array_key_exists($uri, $this->routes_get)){
                    return $this->routes_get[$uri];
                }else{
                    http_response_code(404);
                    echo 'Rota não encontrada';
                }
            }else{
                http_response_code(501);
                echo 'Metodo não suportado';
            }
        }
    }






