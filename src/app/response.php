<?php

    class HttpResponse{
        
        function dispatch($callback){
            $callback();
        }

        static function response($content){
            if(is_array($content)){ 
                echo json_encode($content, JSON_UNESCAPED_SLASHES);
            }else if(is_object($content)){
                echo json_encode(($content),1);
            }else if(is_bool($content)){
                echo json_encode($content);
            }else{  
                print($content);
            }
        }

        static function doTask($function,$body){
            $body = json_decode($body, true);
            if(isset($body['live']) && $body['live']==0){
                unset($body['live']);
                ignore_user_abort(true);
                set_time_limit(0);
                echo 'Status: Solicitação recebida e em processamento';
                header('Connection: close');
                header('Content-Length: '.ob_get_length());
                ob_end_flush();
                flush();
                InitTask::select("$function", $body);
            }else{
                if(isset($body['live'])) unset($body['live']);
                $result = InitTask::select("$function", $body);
                return $result;
            }
        }
    }