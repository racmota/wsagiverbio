<?php
    class HttpRequest{

        private $uri;
        private $app;
        private $method;

        function __construct($app){
            $this->app = $app;
            $this->uri = $_SERVER['REQUEST_URI'];
            $this->method = $_SERVER['REQUEST_METHOD'];
        }

        static function getBody(){
            $body = json_decode(file_get_contents('php://input'),true);
            if(is_array($body)){
                return json_encode($body, JSON_UNESCAPED_SLASHES);
            }else{
                return $body;
            }
        }

        function Resolve(){
            $return = $this->app->routeCollection->findRoute($this->uri, $this->method);
            $response = new HttpResponse();
            $response->dispatch($return);
        }
    }














?>