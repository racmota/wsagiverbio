<?php
    
    require __DIR__.'/../bootstrap/index.php';

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 1000');

    if(session_status() !== 2) {
        session_start();
    }
    $app = new App();    
    
    require __DIR__.'/../routes/routes.php';

    $request = new HttpRequest($app);
    $request->resolve();

