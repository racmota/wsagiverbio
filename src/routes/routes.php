<?php

$app->post('/feriado', function() use($app){
    $feriado = HttpResponse::doTask('feriado', HttpRequest::getBody());
    HttpResponse::response($feriado);
});

$app->post('/client', function() use($app){
    $client = HttpResponse::doTask('client', HttpRequest::getBody());
    HttpResponse::response($client);
});

$app->post('/call', function() use($app){
    $call = HttpResponse::doTask('call', HttpRequest::getBody());
    HttpResponse::response($call);
});

$app->post('/product', function() use($app){
    $product = HttpResponse::doTask('product', HttpRequest::getBody());
    HttpResponse::response($product);
});

$app->post('/metrics', function() use($app){
    $body = HttpRequest::getBody();
    $retorno = HttpResponse::doTask('setCall',$body);
    HttpResponse::response($retorno);
});

$app->post('/login', function() use($app){
    $resultLogin = HttpResponse::doTask('getLogin', HttpRequest::getBody());
    HttpResponse::response($resultLogin);
});

$app->post('/contrato', function() use($app){
    $resultContrato = HttpResponse::doTask('getContrato', HttpRequest::getBody());
    HttpResponse::response($resultContrato);
});

$app->post('/divida', function() use($app){
    $resultDivida = HttpResponse::doTask('getDivida', HttpRequest::getBody());
    HttpResponse::response($resultDivida);
});

$app->post('/proposta', function() use($app){
    $resultProposta = HttpResponse::doTask('getProposta', HttpRequest::getBody());
    HttpResponse::response($resultProposta);
});

$app->post('/all', function() use($app){
    $all = HttpResponse::doTask('all', HttpRequest::getBody());
    HttpResponse::response($all);
});




